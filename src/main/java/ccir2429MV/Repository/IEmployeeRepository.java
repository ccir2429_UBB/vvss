package ccir2429MV.Repository;

import ccir2429MV.Model.Employee;

import java.util.List;

public interface IEmployeeRepository {
    boolean addEmployee(Employee employee);
    void deleteEmployee(Employee employee);
    void modifyEmployee(Employee oldEmployee,Employee newEmployee);
    List<Employee> getEmployeeList();
}
