package ccir2429MV.Repository;

import ccir2429MV.Model.DidacticFunction;
import ccir2429MV.Model.Employee;
import ccir2429MV.Model.Validator.EmployeeValidator;

import java.util.ArrayList;
import java.util.List;

public class EmployeeRepositoryMock implements IEmployeeRepository {
    private List<Employee> employeeList;
    private EmployeeValidator employeeValidator;
    public EmployeeRepositoryMock() {

        employeeValidator = new EmployeeValidator();
        employeeList = new ArrayList<Employee>();

        Employee Ionel   = new Employee("Pacuraru", "1234567890876", DidacticFunction.ASISTENT, 2500,30);
        Employee Mihai   = new Employee("Dumitrescu", "1234567890876", DidacticFunction.LECTURER, 2500,35);
        Employee Ionela  = new Employee("Ionescu", "1234567890876", DidacticFunction.LECTURER, 2500,38);
        Employee Mihaela = new Employee("Pacuraru", "1234567890876", DidacticFunction.ASISTENT, 2500,29);
        Employee Vasile  = new Employee("Georgescu", "1234567890876", DidacticFunction.TEACHER,  3000, 45);
        Employee Marin   = new Employee("Puscas", "1234567890876", DidacticFunction.TEACHER,  2500,67);

        employeeList.add( Ionel );
        employeeList.add( Mihai );
        employeeList.add( Ionela );
        employeeList.add( Mihaela );
        employeeList.add( Vasile );
        employeeList.add( Marin );
    }

    @Override
    public boolean addEmployee(Employee employee) {
        if ( employeeValidator.isValid(employee)) {
            employeeList.add(employee);
            return true;
        }
        return false;
    }

    @Override
    public void deleteEmployee(Employee employee) {
        // TODO Auto-generated method stub
    }

    @Override
    public void modifyEmployee(Employee oldEmployee, Employee newEmployee) {
        int i=0;
        if(oldEmployee==null || newEmployee==null) {
            return;
        }
        else{
            if(oldEmployee.equals(newEmployee)&& oldEmployee.getFunction()==newEmployee.getFunction()&& oldEmployee.getAge()==newEmployee.getAge()){
                return;
            }else
            {
                if(oldEmployee.getCnp()==newEmployee.getCnp()){
                    while(!employeeList.get(i).equals(oldEmployee)){
                        i++;
                    }
                     employeeList.add(i-1,newEmployee);
                }else
                {
                    return;
                }

            }
        }
    }

    @Override
    public List<Employee> getEmployeeList() {
        return employeeList;
    }



}
