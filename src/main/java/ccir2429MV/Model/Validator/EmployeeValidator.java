package ccir2429MV.Model.Validator;

import ccir2429MV.Model.DidacticFunction;
import ccir2429MV.Model.Employee;

public class EmployeeValidator {
    public EmployeeValidator(){}

    public  boolean isValid(Employee employee){
        boolean isLastNameValid  = employee.getLastName().matches("[a-zA-Z]+") && (employee.getLastName().length() > 0);
        boolean isCNPValid       = employee.getCnp().matches("[a-z0-9]+") && (employee.getCnp().length() == 13);
        boolean isFunctionValid  = employee.getFunction().equals(DidacticFunction.ASISTENT) ||
                employee.getFunction().equals(DidacticFunction.LECTURER) ||
                employee.getFunction().equals(DidacticFunction.CONFERENTIAR)||
                employee.getFunction().equals(DidacticFunction.TEACHER);
        boolean isSalaryValid    =  employee.getSalary() > 0;
        boolean isAgeValid = employee.getAge()>0;

        return isLastNameValid && isCNPValid && isFunctionValid && isSalaryValid && isAgeValid;
    }
}
