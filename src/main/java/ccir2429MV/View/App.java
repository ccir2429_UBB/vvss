package ccir2429MV.View;

//Universitatea doreste dezvoltarea unui program pentru gestiunea salariatilor. Acesta va avea urmatoarele functionalitati:
//functionalitati
//F01.	 adaugarea unui nou angajat (nume, prenume, CNP, functia didactica, salariul de incadrare,varsta);
//F02.	 modificarea functiei didactice(nu si a salariului) (asistent/lector/conferentiar/profesor) a unui angajat;
//F03.	 afisarea salariatilor ordonati descrescator dupa salariu apoi crescator dupa varsta.
//F04.   daca un angajat demisioneaza se va putea sterge din lista/baza de date


import ccir2429MV.Controller.EmployeeController;
import ccir2429MV.Model.DidacticFunction;
import ccir2429MV.Model.Employee;
import ccir2429MV.Model.Validator.EmployeeValidator;
import ccir2429MV.Repository.EmployeeRepositoryMock;
import ccir2429MV.Repository.IEmployeeRepository;
import org.apache.commons.lang3.RandomStringUtils;

import java.util.List;

public class App
{
    public static void main( String[] args )
    {
        IEmployeeRepository employeesRepository = new EmployeeRepositoryMock();
        EmployeeController employeeController = new EmployeeController(employeesRepository);

        for(Employee _employee : employeeController.getEmployeesList())
            System.out.println(_employee.toString());
        System.out.println("-----------------------------------------");

        Employee employee = new Employee("LastName", "1234567894321", DidacticFunction.ASISTENT, 2500,30);
        employeeController.addEmployee(employee);

        for(Employee _employee : employeeController.getEmployeesList())
            System.out.println(_employee.toString());

        employeeController.modifyEmployee(new Employee("LastName", "1234567894321", DidacticFunction.ASISTENT, 2500,30),new Employee("LastName", "1234567894321", DidacticFunction.TEACHER, 2500,30));

        for(Employee _employee : employeeController.getEmployeesList())
            System.out.println(_employee.toString());

        EmployeeValidator validator = new EmployeeValidator();
        System.out.println( validator.isValid(new Employee("LastName", "1234567894322", DidacticFunction.TEACHER, 3400,54)) );

        List<Employee> employees = employeeController.getSortedEmployees("salariu");
        System.out.println("---- Employees Sortat by salariu(descrescator):  --------");
        for(Employee em: employees){
            System.out.println(em.toString());
        }
        System.out.println("---------------------------------------");
        employees = employeeController.getSortedEmployees("age");
        System.out.println("---- Employees Sortat by age(crescator):  --------");
        for(Employee em: employees){
            System.out.println(em.toString());
        }
        System.out.println("---------------------------------------");
    }
}
