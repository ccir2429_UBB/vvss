package ccir2429MV.Controller;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import ccir2429MV.Model.Employee;
import ccir2429MV.Repository.IEmployeeRepository;

public class EmployeeController {

    private IEmployeeRepository employeeRepository;

    public EmployeeController(IEmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    public boolean addEmployee(Employee employee) {
        return employeeRepository.addEmployee(employee);
    }

    public List<Employee> getEmployeesList() {
        return employeeRepository.getEmployeeList();
    }

    public void modifyEmployee(Employee oldEmployee, Employee newEmployee) {
        employeeRepository.modifyEmployee(oldEmployee, newEmployee);
    }

    public void deleteEmployee(Employee employee) {
        employeeRepository.deleteEmployee(employee);
    }

    public List<Employee> getSortedEmployees(String criteriu){


       List<Employee> employees= employeeRepository.getEmployeeList();
       if(criteriu.equals("salariu"))
           Collections.sort(employees, new Comparator<Employee>() {
               @Override
               public int compare(Employee e1, Employee e2) {
                   if(e2.getSalary()<e1.getSalary()) return 1;
                   return -1;
               }
           });
       else{
           Collections.sort(employees, new Comparator<Employee>() {
               @Override
               public int compare(Employee e1, Employee e2) {
                   if(e2.getAge()>e1.getAge()) return 1;
                   return -1;
               }
           });
       }
        Collections.reverse(employees);

       return  employees;
    }
}
