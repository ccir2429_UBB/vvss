package ccir2429MV.Repository;

import ccir2429MV.Controller.EmployeeController;
import ccir2429MV.Model.DidacticFunction;
import ccir2429MV.Model.Employee;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertFalse;

public class EmployeeRepositoryTest {

    EmployeeController c;
    EmployeeRepositoryMock rep = new EmployeeRepositoryMock();
    @Before
    public void setUp() throws Exception {
        c=new EmployeeController(rep);
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void T02_WBT_01_Valid(){
        Employee e1=new Employee( "employee1","1234567891234", DidacticFunction.TEACHER,1,20);
        rep.addEmployee(e1);
        int index = rep.getEmployeeList().indexOf(e1);
        Employee newEmp = new Employee( "employee1","1234567891234", DidacticFunction.TEACHER,1000,20);
        assertFalse(rep.getEmployeeList().contains(newEmp));
        rep.modifyEmployee(e1,newEmp);
        assertFalse(c.getEmployeesList().contains(newEmp));
    }

    @Test
    public void T02_WBT_02_Nonvalid(){
        Employee e1=new Employee( "employee1","1234567891234", DidacticFunction.TEACHER,1,20);
        rep.addEmployee(e1);
        Employee newEmp = null;
        rep.modifyEmployee(e1,newEmp);
        assertFalse(rep.getEmployeeList().contains(newEmp));
    }

}