package ccir2429MV.Controller;

import ccir2429MV.Model.DidacticFunction;
import ccir2429MV.Model.Employee;
import ccir2429MV.Repository.EmployeeRepositoryMock;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Random;

import static org.junit.Assert.*;

public class EmployeeControllerTest {

    EmployeeController c;
    @Before
    public void setUp() throws Exception {
        EmployeeRepositoryMock rep = new EmployeeRepositoryMock();
        c=new EmployeeController(rep);
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void addValidEmployee1() {
        //TC1_ECP
        String rndName = RandomStringUtils.randomAlphabetic(5);
        Employee e=new Employee(rndName,RandomStringUtils.randomNumeric(13), DidacticFunction.ASISTENT,1000,20);
        assertNotNull(e);
        assertTrue(c.addEmployee(e));
   }
   @Test
   public void  addValidEmployee2(){
       //TC3_BVA
       Employee e2 = new Employee( RandomStringUtils.randomAlphabetic(1),RandomStringUtils.randomNumeric(13), DidacticFunction.TEACHER,10,20);
       assertNotNull(e2);
       assertTrue(c.addEmployee(e2));

   }
   @Test
   public void addValidEmployee3(){
       //TC4_BVA
       Employee e3 = new Employee( RandomStringUtils.randomAlphabetic(255),RandomStringUtils.randomNumeric(13), DidacticFunction.TEACHER,10,20);
       assertNotNull(e3);
       assertTrue(c.addEmployee(e3));
   }

    @Test
    public void addNonValidEmployee1() {
        //TC2_ECP
        Employee e=new Employee("",RandomStringUtils.randomNumeric(13), DidacticFunction.ASISTENT,1000,20);
        assertNotNull(e);
        assertFalse(c.addEmployee(e));
    }
    @Test
    public void addNonValidEmployee2() {
        //TC3_ECP
        Employee e1=new Employee(RandomStringUtils.randomAlphabetic(5),RandomStringUtils.randomNumeric(13), DidacticFunction.ASISTENT,-100,20);
        assertNotNull(e1);
        assertFalse(c.addEmployee(e1));
    }
    @Test
    public void addNonValidEmployee3() {
        //TC2_BVA
        Employee e2 = new Employee( RandomStringUtils.randomAlphabetic(1),RandomStringUtils.randomNumeric(14), DidacticFunction.TEACHER,10,20);
        assertFalse(c.addEmployee(e2));
    }
    @Test
    public void addNonValidEmployee4() {
        //TC4_BVA
        Employee e3 = new Employee( RandomStringUtils.randomAlphabetic(10),RandomStringUtils.randomNumeric(13), DidacticFunction.TEACHER,-10,20);
        assertFalse(c.addEmployee(e3));
    }

//    @Test
//    public void T02_WBT_01_Valid(){
//        Employee e1=new Employee( "employee1","1234567891234", DidacticFunction.TEACHER,1,20);
//        c.addEmployee(e1);
//        int index = c.getEmployeesList().indexOf(e1);
//        Employee newEmp = new Employee( "employee1","1234567891234", DidacticFunction.TEACHER,1000,20);
//        assertFalse(c.getEmployeesList().contains(newEmp));
//        c.modifyEmployee(e1,newEmp);
//        assertFalse(c.getEmployeesList().contains(newEmp));
//    }
//
//    @Test
//    public void T02_WBT_02_Nonvalid(){
//        Employee e1=new Employee( "employee1","1234567891234", DidacticFunction.TEACHER,1,20);
//        c.addEmployee(e1);
//        Employee newEmp = null;
//        c.modifyEmployee(e1,newEmp);
//        assertFalse(c.getEmployeesList().contains(newEmp));
//    }

}